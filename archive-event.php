<?php
/**
 * The template for displaying archive pages
 *
 * 
 * Template Name: Events listing

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ZooZen_Theme
 * 
 */

get_header();
?>
<div id="content" class="events">

	<main id="primary" class="site-main">
        <?php
                the_title( '<h1 class="entry-title">', '</h1>' ); 
                echo '<div class="intro">';
                the_content();
                echo '</div>';
                events();
        ?>

        </div>

	</main><!-- #main -->
</div>
<?php
// get_sidebar();
get_footer();
