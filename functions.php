<?php
/**
 * ZooZen Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ZooZen_Theme
 */

require_once('post-types/examples.php');
require_once('post-types/testimonial.php');
require_once('post-types/event.php');
// require_once('debug.php');

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'zoozen_theme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function zoozen_theme_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on ZooZen Theme, use a find and replace
		 * to change 'zoozen-theme' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'zoozen-theme', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'zoozen-theme' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'zoozen_theme_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'zoozen_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function zoozen_theme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'zoozen_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'zoozen_theme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function zoozen_theme_widgets_init() {
	$sidebar1 = array(
			'name'          => esc_html__( 'Sidebar', 'zoozen-theme' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'zoozen-theme' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
	);
	$sidebar2 = array (
			'name' 			=> esc_html__( 'Home Widgets', 'zoozen-theme' ),
			'id' 			=> 'homewidgets',
			'description' 	=>  esc_html__( 'Add widgets here.', 'zoozen-theme' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
	);
	$sidebar3 = array (
			'name' 			=> esc_html__( 'Footer Widgets', 'zoozen-theme' ),
			'id' 			=> 'footerwidgets',
			'description' 	=>  esc_html__( 'Add widgets here.', 'zoozen-theme' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
	);
	$sidebar4 = array (
		'name' 			=> esc_html__( 'Home Bottom Widgets', 'zoozen-theme' ),
		'id' 			=> 'homebottomwidgets',
		'description' 	=>  esc_html__( 'Add widgets here.', 'zoozen-theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
);
	register_sidebar($sidebar1);
	register_sidebar($sidebar2);
	register_sidebar($sidebar3);
	register_sidebar($sidebar4);
}
add_action( 'widgets_init', 'zoozen_theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function zoozen_theme_scripts() {
	$testimonialcount = wp_count_posts('testimonial')->publish;
	wp_enqueue_style( 'zoozen-theme-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'zoozen-theme-style', 'rtl', 'replace' );
	// if ( is_page( array( 'blog'))) {
	// 	wp_enqueue_script( 'zoozen-theme-isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array('jquery'), 1.1, true );
	// 	wp_enqueue_script( 'zoozen-theme-blog', get_template_directory_uri() . '/js/blog.js', array(), _S_VERSION, true );
	// }
	wp_enqueue_script( 'zoozen-theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'zoozen-theme-custom', get_template_directory_uri() . '/js/zoozen.js', array(), _S_VERSION, true );
	wp_localize_script( 'zoozen-theme-custom', 'testimonialcount', $testimonialcount);
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'zoozen_theme_scripts' );
function add_admin_scripts( $hook ) {

    global $post;

    if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
		wp_enqueue_script(  'copyscript', get_stylesheet_directory_uri().'/js/zoozen-edit.js' );
    }
}
add_action( 'admin_enqueue_scripts', 'add_admin_scripts', 10, 1 );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Custom work here
 */


// add sidebar class to body
function wpse_77719_sidebar_body_class( $classes )
{
    $classes[] = wp_get_sidebars_widgets() ? 'has-sidebar' : 'no-sidebar';
    return $classes;
}
add_filter( 'body_class', 'wpse_77719_sidebar_body_class' );

// debug: show the used template
function meks_which_template_is_loaded() {
	if ( is_super_admin() ) {
		global $template;
		print_r( $template );
	}
}
add_action( 'wp_footer', 'meks_which_template_is_loaded' );
 
add_image_size( 'blogitem', 700, 0 );
add_image_size( 'tiny' , 70, 70, true );



// register two extra menu's
if ( ! function_exists( 'zoozen_register_nav_menu' ) ) {
    function zoozen_register_nav_menu(){
        register_nav_menus( array(
            'top_menu' => __( 'Secondary Menu', 'zoozen-theme' ),
			'footer_menu'  => __( 'Footer Menu', 'zoozen-theme' ),
			'mobile_menu' => __( 'Mobile Menu', 'zoozen-theme' ),
        ) );
    }
    add_action( 'after_setup_theme', 'zoozen_register_nav_menu', 0 );
}



function bloglist($amount) {
	$args = array (
		'post-type' => 'post',
		'posts_per_page' => $amount,
	);
	$blog_query = new WP_Query( $args );
	if ($blog_query->have_posts() ) {
		echo '<div class="blog-wrapper">';
		while ( $blog_query->have_posts() ) {
			$blog_query->the_post();
			$id = get_the_ID();
			$link = get_permalink($id);
			$title = get_the_title();
			$featured_img_url = get_the_post_thumbnail_url($id, 'medium'); 
			echo '<div class="grid-item">';
			echo '<a href="' . $link . '">';
			echo '<img src="'. $featured_img_url . '" alt="' . $title . '" />';
			echo the_title('<p>', '</p>');
			echo '</a>';
			echo '</div>';
		}
		echo '</div>';
		echo '<div class="bloglink"><a class="redbutton" href="/blog/">Vind meer inspiratie</a></div>';
	}
}



/**
 * Adds a meta box to the post editing screen
 */
function zoozen_custom_meta() {
	add_meta_box( 'zoozen_meta', 
			__( 'Testimonials', 'zoozen-theme' ), 
			'zoozen_meta_callback', 
			array('page', 'post'),
			'side' );
}
add_action( 'add_meta_boxes', 'zoozen_custom_meta' );

/**
 * Outputs the content of the meta box
 */
function zoozen_meta_callback( $post ) {
	$args = array(
		'post_type' => 'testimonial',
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);
	$query = new WP_Query( $args );
	echo 'Copy to use in Shortcode'; 
	$content = '<ul>';
	if ( $query->have_posts() ) {
		while( $query->have_posts() ) {
			$query->the_post();
			$title = get_the_title();
			$post_id = get_the_ID();
			$content .= '<li>' . $title . '<br> <code id="' . $post_id . '">[testimonial pageid=' . $post_id . ']</code>';
			$content .= '<button onclick="copyToClipboard(\'#'.$post_id.'\')">Copy</button></li>';
		}
	}
	$content .= '</ul>';
	echo $content;

	 
}

//Modern Jquery
add_action('wp_enqueue_scripts', 'nwd_modern_jquery');
function nwd_modern_jquery() {
    global $wp_scripts;
    if(is_admin()) return;
    $wp_scripts->registered['jquery-core']->src = get_stylesheet_directory_uri() .'/js/jquery-3.5.1.min.js';
    $wp_scripts->registered['jquery']->deps = ['jquery-core'];
}

// add_action('wp_head', 'output_all_postmeta' );
function output_all_postmeta() {

	$postmetas = get_post_meta(get_the_ID());

	foreach($postmetas as $meta_key=>$meta_value) {
		echo $meta_key . ' : ' . $meta_value[0] . '<br/>';
	}
}

 // banner size
 add_image_size( 'banner', 1280, 960 );

function banner() {
	echo '<div id="banner">';
	echo '<div class="banner-wrapper">';
	echo '<div class="banner-image">';

	// if on template front-page.php get the image from customizer
	global $template;

	if ( basename( $template ) === 'front-page.php' ) :
		echo get_header_image_tag(); 
	else :		
		// else get the featured image
		zoozen_theme_post_thumbnail();
	endif;
	echo '</div></div></div>';
}