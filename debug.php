<?php

// That's not that easy to read:
var_dump( get_required_files() );

/** 
 * Show template hierarchy and theme at the end of the request/page.
 * @return void
 */
function wpse31909_template_info()
{
        // Don't display for non-admin users
        if ( ! current_user_can( 'manage_options' ) )
            return;

        // You have to build yourself the hierarchy here or somewhere in front of the fn
        global $wp_template_hierarchy;

        $content  = '<pre>';
            // Show template hierarchy
            $content .= "TEMPLATE HIERARCHY\n==================\n";
            $content .= var_export( $wp_template_hierarchy, true );

            // Show current theme in use:
            $content .= "\n\nCURRENT THEME\n=============\n";
            $content .= var_export( get_option( 'template' ), true );
            // or: 
            # $content .= var_export( get_template(), true );
        $content .= '</pre>';

        return print $content;
}
add_action( 'shutdown', 'wpse31909_template_info' );


/**
 * Show template on singular views attached to the end of the content for admins
 * @return $content
 */
function wpse31909_template_to_content( $content )
{
        // Display standard content for non-admin users and not single post/page/cpt/attachment view.
        if ( ! current_user_can( 'manage_options' ) && ! is_singular() )
            return $content;

        $content .= '<pre>';
            // Show current template in use: Must be in the loop to get the global $post
           $content .= var_export( get_post_meta( $GLOBALS['post']->ID, '_wp_page_template' ), true );
        $content .= '</pre>';

        return $content;
}
add_filter( 'the_content', 'wpse31909_template_to_content' );