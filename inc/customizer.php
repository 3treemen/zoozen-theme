<?php
/**
 * ZooZen Theme Theme Customizer
 *
 * @package ZooZen_Theme
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function zoozen_theme_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'zoozen_theme_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'zoozen_theme_customize_partial_blogdescription',
			)
		);
	}
	// Add Social Media Section
	$wp_customize->add_section(
		'social-media',
		array(
			'title' => __( 'Social Media', '_s' ),
			'priority' => 30,
			'description' => __( 'Enter the URL to your account for each service for the icon to appear in the header.', '_s' )
		)
	);
	// Add Twitter Setting
	$wp_customize->add_setting( 'twitter', array( 'default' => '' ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'twitter', array( 'label' => __( 'Twitter', '_s' ), 'section' => 'social-media', 'settings' => 'twitter', ) ) );

	// Add FaceBook Setting
	$wp_customize->add_setting( 'facebook', array( 'default' => '' ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'facebook', array( 'label' => __( 'Facebook', '_s' ), 'section' => 'social-media', 'settings' => 'facebook', ) ) );

	// Add Instagram Setting
	$wp_customize->add_setting( 'instagram', array( 'default' => '' ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'instagram', array( 'label' => __( 'Instagram', '_s' ), 'section' => 'social-media', 'settings' => 'instagram', ) ) );

	// Add Vimeo Setting
	$wp_customize->add_setting( 'vimeo', array( 'default' => '' ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'vimeo', array( 'label' => __( 'Vimeo', '_s' ), 'section' => 'social-media', 'settings' => 'vimeo', ) ) );
	
	// Add YouTube Setting
	$wp_customize->add_setting( 'youtube', array( 'default' => '' ) );
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'youtube', array( 'label' => __( 'YouTube', '_s' ), 'section' => 'social-media', 'settings' => 'youtube', ) ) );

}
add_action( 'customize_register', 'zoozen_theme_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function zoozen_theme_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function zoozen_theme_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function zoozen_theme_customize_preview_js() {
	wp_enqueue_script( 'zoozen-theme-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), _S_VERSION, true );
}
add_action( 'customize_preview_init', 'zoozen_theme_customize_preview_js' );
