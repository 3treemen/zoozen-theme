<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ZooZen_Theme
 */

?>

	<footer class="site-footer">
		<div class="site-info">
			<?php dynamic_sidebar( 'footerwidgets' ); ?>
		</div><!-- .site-info -->
		<!-- footer menu -->
		<div class="footer-menu">
			<?php
					wp_nav_menu(
						array(
							'theme_location' => 'footer_menu',
						)
					)
					?>
		</div>
		<div class="copy">
							Aikidao &copy; 2021  - website: <a href="https://zoo.nl">Studio ZOO</a>

		</div>
	</footer><!-- #colophon -->
	<div id="mobileMenu" class="modal">
	<span class="close">&times;</span>
		<div class="modal-content">

			<?php
				wp_nav_menu(
					array(
						'theme_location' => 'mobile_menu',
						'menu_id'        => 'mobile_menu',
					)
				)
				?>
		</div>
	</div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
