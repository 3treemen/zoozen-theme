<?php
/*
Template Name: Post Archives
*/

get_header();
?>
	<main id="primary" class="site-main">
<?php

$args = array(
    'posts_per_page'   => -1,
    'post_type'        => 'post',
);
$the_query = new WP_Query( $args );
    ?>
    <div class="grid">
    <!-- <div class="grid-sizer"></div> -->
        <?php
    while ( $the_query->have_posts() ) {
        $the_query->the_post();
        $link = get_permalink($post->ID);
        $featured_img_url = get_the_post_thumbnail_url($post->ID, 'blogitem'); 
        $excerpt = '';
        if (has_excerpt()) {
            $excerpt = wp_strip_all_tags(get_the_excerpt());
        }
        echo '<div class="grid-item">';
        echo '<a href="' . $link . '">';
        echo '<img src="'. $featured_img_url . '" />';
        echo '<div>';
        echo the_title('<h2>', '</h2>');
        echo '<p>' . $excerpt . '</p>';
        echo '<p class="readmore">Lees verder &rarr;</p>';
        echo '</div>';
        echo '</a>';
        echo '</div>';
    }
    echo '</div>';
?>

</main>


<?php
get_footer();
