<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 *  * Template Name: Front Page

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ZooZen_Theme
 */

get_header();
?>
<div id="home-quote">
	<div class="container">
		<p>'What lies behind us and what lies before us are tiny matters 
			compared to what lies whitin us.'</p>
		<i>- Ralph Waldo Emerson -</i>
	</div> 
</div>


<div id="content">
	<main id="primary" class="site-main">

		<div id="home-widgets">
			<?php dynamic_sidebar( 'homewidgets' ); ?>
		</div>		
		
			<?php //events(); ?>
		
		<div id="testimonials">
			<?php home_testimonials(); ?>
		</div>
		<div id="blog">
			<?php bloglist(3); ?>
		</div>
		<div id="homebottom">
			<?php dynamic_sidebar( 'homebottomwidgets' ); ?>
		</div>
	</main><!-- #main -->
</div>
<?php
get_footer();
