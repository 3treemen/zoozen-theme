<?php

/**
 * Registers the `testimonial` post type.
 */
function testimonial_init() {
	register_post_type( 'testimonial', array(
		'labels'                => array(
			'name'                  => __( 'Testimonials', 'zoozen-theme' ),
			'singular_name'         => __( 'Testimonial', 'zoozen-theme' ),
			'all_items'             => __( 'All Testimonials', 'zoozen-theme' ),
			'archives'              => __( 'Testimonial Archives', 'zoozen-theme' ),
			'attributes'            => __( 'Testimonial Attributes', 'zoozen-theme' ),
			'insert_into_item'      => __( 'Insert into Testimonial', 'zoozen-theme' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'zoozen-theme' ),
			'featured_image'        => _x( 'Featured Image', 'testimonial', 'zoozen-theme' ),
			'set_featured_image'    => _x( 'Set featured image', 'testimonial', 'zoozen-theme' ),
			'remove_featured_image' => _x( 'Remove featured image', 'testimonial', 'zoozen-theme' ),
			'use_featured_image'    => _x( 'Use as featured image', 'testimonial', 'zoozen-theme' ),
			'filter_items_list'     => __( 'Filter Testimonials list', 'zoozen-theme' ),
			'items_list_navigation' => __( 'Testimonials list navigation', 'zoozen-theme' ),
			'items_list'            => __( 'Testimonials list', 'zoozen-theme' ),
			'new_item'              => __( 'New Testimonial', 'zoozen-theme' ),
			'add_new'               => __( 'Add New', 'zoozen-theme' ),
			'add_new_item'          => __( 'Add New Testimonial', 'zoozen-theme' ),
			'edit_item'             => __( 'Edit Testimonial', 'zoozen-theme' ),
			'view_item'             => __( 'View Testimonial', 'zoozen-theme' ),
			'view_items'            => __( 'View Testimonials', 'zoozen-theme' ),
			'search_items'          => __( 'Search Testimonials', 'zoozen-theme' ),
			'not_found'             => __( 'No Testimonials found', 'zoozen-theme' ),
			'not_found_in_trash'    => __( 'No Testimonials found in trash', 'zoozen-theme' ),
			'parent_item_colon'     => __( 'Parent Testimonial:', 'zoozen-theme' ),
			'menu_name'             => __( 'Testimonials', 'zoozen-theme' ),
		),
		'public'                => true,
		'hierarchical'          => true,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'thumbnail'),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'testimonial',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'testimonial_init' );

/**
 * Sets the post updated messages for the `testimonial` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `testimonial` post type.
 */
function testimonial_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['testimonial'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Testimonial updated. <a target="_blank" href="%s">View Testimonial</a>', 'zoozen-theme' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'zoozen-theme' ),
		3  => __( 'Custom field deleted.', 'zoozen-theme' ),
		4  => __( 'Testimonial updated.', 'zoozen-theme' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Testimonial restored to revision from %s', 'zoozen-theme' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Testimonial published. <a href="%s">View Testimonial</a>', 'zoozen-theme' ), esc_url( $permalink ) ),
		7  => __( 'Testimonial saved.', 'zoozen-theme' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Testimonial submitted. <a target="_blank" href="%s">Preview Testimonial</a>', 'zoozen-theme' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Testimonial scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Testimonial</a>', 'zoozen-theme' ),
		date_i18n( __( 'M j, Y @ G:i', 'zoozen-theme' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Testimonial draft updated. <a target="_blank" href="%s">Preview Testimonial</a>', 'zoozen-theme' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'testimonial_updated_messages' );



function testimonial_register_meta_boxes( $meta_boxes ) {
    $prefix = 'testimonial-';

    $meta_boxes[] = [
        'title'      => esc_html__( 'Testimonial', 'zoozen-theme' ),
        'id'         => 'testimonial_meta',
        'post_types' => ['testimonial'],
        'context'    => 'normal',
        'priority'   => 'high',
        'fields'     => [
            [
                'type' => 'text',
                'name' => esc_html__( 'Naam', 'zoozen-theme' ),
                'id'   => $prefix . 'naam',
            ],
            [
                'type' => 'text',
                'id'   => $prefix . 'functie',
                'name' => esc_html__( 'Functie', 'zoozen-theme' ),
			],
			[
				'type' => 'textarea',
				'id'   => $prefix . 'quote',
				'name' => esc_html__( 'Quote', 'zoozen-theme' ),
			],
			[
                'type' => 'checkbox',
                'id'   => $prefix . 'homepage',
                'name' => esc_html__( 'Show on Homepage', 'zoozen-theme' ),
            ],
        ],
    ];

    return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'testimonial_register_meta_boxes' );


function home_testimonials() {
	$args = array ( 
		'post_type' => 'testimonial',
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'posts_per_page' => -1,
		'meta_query' => array(
			array(
				'key' => 'testimonial-homepage',
				'value' => 1,
			),
		)
	  );
	  $query = new WP_Query( $args );
	  $testimonialcount = wp_count_posts('testimonial')->publish;

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
		  $query->the_post();
		  $functie = rwmb_meta('testimonial-functie');
		  $naam = rwmb_meta('testimonial-naam' );
		  $quote = rwmb_meta('testimonial-quote');
		  $title = get_the_title();
		  $featured_image_url = get_the_post_thumbnail_url(get_the_ID(), 'tiny');
		  echo "<div class='testimonial'>";
			echo '<div>';
			echo the_title('<h3>', '</h3>');
			echo '<p class="quote">' . $quote . '</p></div>';
			echo '<div class="persoon">';  
			if($featured_image_url){
				echo '<div class="portret" style="background-image:url('.$featured_image_url.');"></div>';
			}
			echo '<p>' . $naam . '</p>';
			echo '<p>' . $functie . '</p>';
			echo '</div>';
		  echo '</div>';

		}
		echo '<a class="more_testimonials" href="/testimonials">Meer ('. $testimonialcount . ') testimonials</a>';
	  }
	  wp_reset_postdata();
}


function single_testimonial($atts) {
	$atts = shortcode_atts( array(
		'pageid' => '', 
	), $atts );

	extract( $atts );

	$content = '';

	$query_args = array(
		'post_type' => 'testimonial',
		'page_id'	=> $pageid,
		'posts_per_page' => 1
	);

	$query = new WP_Query($query_args);
	if($query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
		  	$functie = rwmb_meta('testimonial-functie');
		  	$naam = rwmb_meta('testimonial-naam' );
		  	$quote = rwmb_meta('testimonial-quote');
		  	$title = get_the_title();
		  	$featured_image_url = get_the_post_thumbnail_url(get_the_ID(), 'tiny');

			$content .= "<div class='testimonial block'>";
			$content .= '<div>';
			$content .= get_the_title('<h5>', '</h5>');
			$content .= '<p class="quote">' . $quote . '</p></div>';
			$content .= '<div class="persoon">';  
			$content .= '<div class="portret" style="background-image:url('.$featured_image_url.');"></div>';
			$content .= '<p>' . $naam . '</p>';
			$content .= '<p>' . $functie . '</p>';
			$content .= '</div>';
			$content .= '</div>';
		}

		wp_reset_postdata();
	  }
	  return $content;

}
add_shortcode( 'testimonial', 'single_testimonial');


function testimonials() {
	$args = array ( 
		'post_type' => 'testimonial',
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'posts_per_page' => -1,
	
	);
	$query = new WP_Query( $args );
	echo '<div class="grid">';
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$functie = rwmb_meta('testimonial-functie');
			$naam = rwmb_meta('testimonial-naam' );
			$quote = rwmb_meta('testimonial-quote');
			$title = get_the_title();
			$featured_image_url = get_the_post_thumbnail_url(get_the_ID(), 'tiny');
		  	echo "<div class='grid-item'>";
			echo the_title('<h3>', '</h3>');
			if($quote) {
				echo '<div class="quote">' . $quote . '</div>';
			}
			echo '<div class="persoon">';  
			if($featured_image_url) {
				echo '<img class="portret" src="'.$featured_image_url.'" alt="'.$naam.'" />';
			}
			if($naam) {
				echo '<p>' . $naam . '</p>';
			}
			if($functie) {
				echo '<p>' . $functie . '</p>';
			}
			echo '</div>';
		echo '</div>';
		}
	  }
	  wp_reset_postdata();
}
