<?php

/**
 * Registers the `event` post type.
 */
function event_init() {
	register_post_type( 'event', array(
		'labels'                => array(
			'name'                  => __( 'Events', 'zoozen-theme' ),
			'singular_name'         => __( 'Event', 'zoozen-theme' ),
			'all_items'             => __( 'All Events', 'zoozen-theme' ),
			'archives'              => __( 'Event Archives', 'zoozen-theme' ),
			'attributes'            => __( 'Event Attributes', 'zoozen-theme' ),
			'insert_into_item'      => __( 'Insert into Event', 'zoozen-theme' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Event', 'zoozen-theme' ),
			'featured_image'        => _x( 'Featured Image', 'event', 'zoozen-theme' ),
			'set_featured_image'    => _x( 'Set featured image', 'event', 'zoozen-theme' ),
			'remove_featured_image' => _x( 'Remove featured image', 'event', 'zoozen-theme' ),
			'use_featured_image'    => _x( 'Use as featured image', 'event', 'zoozen-theme' ),
			'filter_items_list'     => __( 'Filter Events list', 'zoozen-theme' ),
			'items_list_navigation' => __( 'Events list navigation', 'zoozen-theme' ),
			'items_list'            => __( 'Events list', 'zoozen-theme' ),
			'new_item'              => __( 'New Event', 'zoozen-theme' ),
			'add_new'               => __( 'Add New', 'zoozen-theme' ),
			'add_new_item'          => __( 'Add New Event', 'zoozen-theme' ),
			'edit_item'             => __( 'Edit Event', 'zoozen-theme' ),
			'view_item'             => __( 'View Event', 'zoozen-theme' ),
			'view_items'            => __( 'View Events', 'zoozen-theme' ),
			'search_items'          => __( 'Search Events', 'zoozen-theme' ),
			'not_found'             => __( 'No Events found', 'zoozen-theme' ),
			'not_found_in_trash'    => __( 'No Events found in trash', 'zoozen-theme' ),
			'parent_item_colon'     => __( 'Parent Event:', 'zoozen-theme' ),
			'menu_name'             => __( 'Events', 'zoozen-theme' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'event',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'event_init' );

/**
 * Sets the post updated messages for the `event` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `event` post type.
 */
function event_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['event'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Event updated. <a target="_blank" href="%s">View Event</a>', 'zoozen-theme' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'zoozen-theme' ),
		3  => __( 'Custom field deleted.', 'zoozen-theme' ),
		4  => __( 'Event updated.', 'zoozen-theme' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Event restored to revision from %s', 'zoozen-theme' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Event published. <a href="%s">View Event</a>', 'zoozen-theme' ), esc_url( $permalink ) ),
		7  => __( 'Event saved.', 'zoozen-theme' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Event submitted. <a target="_blank" href="%s">Preview Event</a>', 'zoozen-theme' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Event scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Event</a>', 'zoozen-theme' ),
		date_i18n( __( 'M j, Y @ G:i', 'zoozen-theme' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Event draft updated. <a target="_blank" href="%s">Preview Event</a>', 'zoozen-theme' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'event_updated_messages' );


add_filter( 'rwmb_meta_boxes', 'event_register_meta_boxes' );

function event_register_meta_boxes( $meta_boxes ) {
    $prefix = 'event-';

    $meta_boxes[] = [
        'title'      => esc_html__( 'Event', 'online-generator' ),
        'id'         => 'event',
        'post_types' => ['event'],
        'context'    => 'normal',
        'priority'   => 'high',
        'fields'     => [
            [
                'type' => 'datetime',
                'id'   => $prefix . 'event_start',
				'name' => esc_html__( 'Event start', 'online-generator' ),
				'js_options' => array(
					'dateFormat' 	=> 'dd-mm-yy',
					'showTimepicker' => true,
				),
				'save_format' => 'Y-m-d H:i:s',
            ],
            [
                'type' => 'datetime',
                'id'   => $prefix . 'event_end',
				'name' => esc_html__( 'Event end', 'online-generator' ),
				'js_options' => array(
					'dateFormat' 	=> 'dd-mm-yy',
					'showTimepicker' => true,
				),
				'save_format' => 'Y-m-d H:i:s',
            ],
            [
                'type' => 'textarea',
                'id'   => $prefix . 'event_location',
                'name' => esc_html__( 'Location', 'online-generator' ),
            ],
        ],
    ];

    return $meta_boxes;
}

function events() {
	$args = array(
		'post_type' => 'event',
		'posts_per_page' => -1,
		'order' => 'ASC',
	);

	$query = new WP_Query( $args );
	echo '<div id="events">';
	echo '<div class="event-list">';
	if ( $query->have_posts() ) {
		echo '<h2>Geplande evenementen</h2>';
		while ( $query->have_posts() ) {
			$query->the_post();
			$curdate = date_i18n('j F Y H:i');
			$start = rwmb_meta('event-event_start');
			$startdate = date_i18n( 'j F Y  H:i', strtotime($start));
			$end = rwmb_meta('event-event_end');
			$enddate = date_i18n( 'j F Y H:i', strtotime($end));
			$location = rwmb_meta('event-event_location');
			$featured_image_url = get_the_post_thumbnail_url(get_the_ID(), 'thumbnail');
			$permalink = get_the_permalink();
				echo '<a href="' . $permalink . '" class="event">';
				if($featured_image_url){
					echo '<div class="eventimage" style="background-image:url('.$featured_image_url.');"></div>';
				}
				echo '<div class="eventdetails">';
				echo '<div class="dates">';
				echo $startdate;
				if($enddate) {
					echo ' - ' .$enddate;
				}
				echo '</div>';
				echo the_title('<h3>', '</h3>' );
				echo '<p>' . $location . '</p>';
				echo '</div></a>';
			
		}
	}
	echo '</div>';
	echo '</div>';
	wp_reset_postdata();
}

