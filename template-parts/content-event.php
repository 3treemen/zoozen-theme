<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ZooZen_Theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php	
			the_title( '<h1 class="entry-title">', '</h1>' );
	?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'zoozen-theme' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			)
		);

		$start = rwmb_meta('event-event_start');
		$startdate = date_i18n( 'j F Y  H:i', strtotime($start));
		$end = rwmb_meta('event-event_end');
		$enddate = date_i18n( 'j F Y H:i', strtotime($end));
		$location = rwmb_meta('event-event_location');
		echo '<dl>';
		if($start) {
			echo '<dt>Aanvang:</dt><dd>' . $startdate .'</dd>';
		}
		if($end) {
			echo '<dt>Einde:</dt><dd>' . $enddate . '</dd>';
		}
		if($location) {
			echo '<dt>Locatie:</dt><dd>' . $location . '</dd>';
		}
		echo '</dl>';

?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php zoozen_theme_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
